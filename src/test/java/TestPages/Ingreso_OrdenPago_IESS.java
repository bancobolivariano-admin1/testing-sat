package TestPages;

import Globales.CapturaPantalla;
import Globales.Reporte;
import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.Map;

public class Ingreso_OrdenPago_IESS {

    private Map<String, Object> vars;
    JavascriptExecutor js;
    CapturaPantalla print = new CapturaPantalla();

    public void click_link_pagos()
    {
        Util.waitForElementToBeClickable(By.linkText("Pagos"));
        Util.driver.findElement(By.linkText("Pagos")).click();
    }

    public void click_link_carga()
    {
        Util.driver.switchTo().defaultContent();
        Util.driver.switchTo().frame(1);
        Util.waitForElementToBeClickable(By.id("306"));
        WebElement carga = Util.driver.findElement(By.id("306"));
        String actual = carga.getText();
        Util.assert_igual("SUBMENU", "Verificación de etiqueta", actual, "Carga/Transmision", false, "N");
        carga.click();
    }
    public void click_link_pagoiess()
    {
        Util.driver.switchTo().defaultContent();
        Util.driver.switchTo().frame(2);
        //Util.driver.findElement(By.linkText("Pago IESS")).click();
        WebElement carga = Util.driver.findElement(By.linkText("Pago IESS"));
        String actual = carga.getText();
        Util.assert_contiene("SUBMENU","Verificacion de etiqueta",actual,"Pago IESS",false,"N");
        carga.click();
    }
    public void selec_servicio(String nom_servicio)
    {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        Util.waitForElementToBeClickable(By.name("cbo_servicio"));
        WebElement cbo_servicio1 = Util.driver.findElement(By.name("cbo_servicio"));
        Select servicio = new Select(cbo_servicio1) ;
        servicio.selectByVisibleText(nom_servicio);
        Util.assert_contiene("ORDEN DE PAGO", "Selecciona servicio", nom_servicio, "", false, "N");

    }
    public void ingresa_diapago(String d)
    {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        Util.waitForElementToBeClickable(By.name("txt_fecinipag_dd"));
        WebElement dia=Util.driver.findElement(By.name("txt_fecinipag_dd"));
        dia.click();
        dia.sendKeys(d);

    }
    public void ingresa_mespago( String m)
    {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }

        WebElement mes=Util.driver.findElement(By.name("cbo_fecinipag_mm"));
        Select mes1=new Select(mes);
        mes1.selectByVisibleText(m);
    }
    public void ingresa_aniopago(String a)
    {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }

        WebElement anio=Util.driver.findElement(By.name("txt_fecinipag_aa"));
        anio.click();
        anio.sendKeys(a);
        String fecha="Fecha de pago programada";
        Reporte.agregarPaso("ORDEN DE PAGO","Ingreso de fecha de pago",fecha,"",false,"N");

    }

    public void selec_cuenta(String num_cuenta)
    {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        Util.waitForElementToBeClickable(By.name("cbo_ctaautpg"));
        WebElement cbo_cuenta = Util.driver.findElement(By.name("cbo_ctaautpg"));
        Select cuenta = new Select(cbo_cuenta) ;
        cuenta.selectByVisibleText(num_cuenta);
        Util.assert_contiene("ORDEN DE PAGO", "Selecciona cuenta", num_cuenta, "", false, "N");
    }
    public void ingresa_referencia(String ref)
    {
        // Util.waitForElementToBeClickable(By.name("txt_referencia"));
        WebElement txt_referencia = Util.driver.findElement(By.name("txt_referencia"));
        txt_referencia.click();
        txt_referencia.sendKeys(ref);
        Reporte.agregarPaso("ORDEN DE PAGO", "Ingreso referencia", ref, "", false, "N");
        Util.AvanzarPagina();
    }

    public void clic_btningresar()
    {
        WebElement ingresarorden= Util.driver.findElement(By.name("btnBoton3"));
        String actual = ingresarorden.getAttribute("value");
        Util.assert_igual("ORDEN DE PAGO", "Click en boton Generar Orden", actual, "Ingresar datos", true, "N");
        ingresarorden.click();
        Util.AvanzarPagina();
    }

    public void ingresaruc(String ruc)
    {
        WebElement ruc_bene=Util.driver.findElement(By.name("txt_ruc"));
        ruc_bene.click();
        ruc_bene.sendKeys(ruc);
        Reporte.agregarPaso("ORDEN DE PAGO","Ingresa Ruc beneficiario",ruc,"",false,"N");

    }
    public void Seleccionatipobusqueda(String a){
        WebElement buscar =Util.driver.findElement(By.name("cbo_tip_busqueda"));
        Select tipo = new Select(buscar) ;
        tipo.selectByVisibleText(a);
        Util.assert_contiene("ORDEN DE PAGO", "Selecciona tipo de busqueda", a, "", false, "N");

    }
    public void ingresaconcepto(String concepto){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        WebElement concep = Util.driver.findElement(By.id("txt_auto_concepto"));
        concep.clear();
        concep.sendKeys(concepto);
        concep.sendKeys("\t"); //comando para dar tab
        Util.assert_contiene("ORDEN DE PAGO", "Ingresa concepto", concepto, "", false, "N");

    }
    public void ingresarsucursal(String sucursal){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        WebElement suc= Util.driver.findElement(By.name("txt_sucursal"));
        suc.click();
        suc.sendKeys(sucursal);
        Reporte.agregarPaso("ORDEN DE PAGO","Ingresa sucursal",sucursal,"",false,"N");

    }
    public  void ingresacomprobante(String comp){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        WebElement comprobante= Util.driver.findElement(By.name("txt_comprobante"));
        comprobante.click();
        comprobante.sendKeys(comp);
        Reporte.agregarPaso("ORDEN DE PAGO","Ingresa comprobante",comp,"",false,"N");

    }
    public void consultodeuda(){
        Util.RetrocederPagina();
        Util.waitForElementToBeClickable(By.name("btnBoton4"));
        WebElement btn_consultar=Util.driver.findElement(By.name("btnBoton4"));
        String actual = btn_consultar.getAttribute("value");
        Util.assert_igual("ORDEN DE PAGO", "Click en botón Consultar", actual, "Consultar", true, "N");
        btn_consultar.click();
        Util.AvanzarPagina();
    }

    public void marcodeuda(){
        WebElement chk_deuda= Util.driver.findElement(By.name("chk_ord"));
        chk_deuda.click();
        Reporte.agregarPaso("ORDEN DE PAGO","Selecciona deuda","Deuda marcada","",true,"N");
    }

    public void geraorden(){
        WebElement btnGenerar= Util.driver.findElement(By.name("btnBoton5"));
        String actual = btnGenerar.getAttribute("value");
        Util.assert_igual("ORDEN DE PAGO", "Click en boton Generar Orden", actual, "Generar Orden", false, "N");
        btnGenerar.click();
        Util.AvanzarPagina();
    }
    public void vp_mensaje()
    {
        Util.driver.findElement(By.id("cuerpo")).click();
        Util.AvanzarPagina();

        boolean mensaje= Util.driver.getPageSource().contains("Verificado O.K.");
        if (mensaje)
            Reporte.agregarPaso("ORDEN DE PAGO", "Verificacion de mensaje de confirmacion", "Orden ingresada exitosamente", "", true, "N");
        else
            Reporte.agregarPaso("ORDEN DE PAGO", "Verificacion de mensaje de confirmacion", "ERROR", "", true, 1, "N");
        //como validar que la acción en la table ID=TABLE_CONSULTA_CARGA el resultado es Verificado O.K.
    }




}
