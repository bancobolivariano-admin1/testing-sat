package TestPages;

import Globales.CapturaPantalla;

import Globales.Reporte;
import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.Map;

public class Bloqueo_Cheques_24online {

    //private WebDriver driver;
    private Map<String, Object> vars;
    JavascriptExecutor js;
    CapturaPantalla print = new CapturaPantalla();

public void Bloqueo_Cheque(WebDriver driver) throws InterruptedException {
        driver.findElement(By.linkText("24online")).click();
        driver.switchTo().defaultContent();
        driver.switchTo().frame(1);
        Thread.sleep(4000);
        driver.findElement(By.id("1001")).click();
        driver.switchTo().defaultContent();
        driver.switchTo().frame(2);
        driver.findElement(By.linkText("Bloqueo de Cheques")).click();
        Select cuenta= new Select(driver.findElement(By.id("id_cuentas"))) ;
        cuenta.selectByVisibleText("000XXXX359-NOMBRE: 27-CTE-USD");
        Thread.sleep(5000);
       // Reporte.agregarPaso("Bloqueo de cheque", "Ingreso de cuenta", Util.getDataCliente()[1], "", true);
        Select accion= new Select(driver.findElement(By.name("cbo_tipo_anula")));
        accion.selectByVisibleText("Revocatoria Transitoria");
        Thread.sleep(5000);

        driver.findElement(By.name("txt_num_cheque")).sendKeys("193616");
        driver.findElement(By.name("txt_val_cheque")).sendKeys("10");
        Select causa= new Select(driver.findElement(By.name("cbo_causa_anula")));
        causa.selectByVisibleText("Otras causas");

        Select girardo= new Select(driver.findElement(By.name("cbo_lugar_giro")));
        girardo.selectByVisibleText("Ecuador");

        driver.findElement(By.name("txt_nombre_benef")).sendKeys("Karla Merino");
        driver.findElement(By.name("txt_comentario")).sendKeys("Prueba automatica");

        Thread.sleep(5000);
        //Boton continuar
        driver.findElement(By.name("btn_boton")).click();

        Thread.sleep(4000);
        WebElement acepto = driver.findElement(By.name("chk_acepto"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].checked = true;", acepto);

        //Boton aceptar
        driver.findElement(By.xpath("//*[@id=\"ejc_transaccion\"]/div/div[2]/form/table/tbody/tr[2]/td/div[2]/nobr/input[2]")).click();

        driver.findElement(By.id("txttoken")).sendKeys("1111");
        //Boton confirmar
        driver.findElement(By.xpath("//*[@id=\"ejc_transaccion\"]/div/div[2]/form/table/tbody/tr[2]/td/div[2]/nobr/input[2]")).click();

    }
}

