package TestPages;
import Globales.CapturaPantalla;
import Globales.Reporte;
import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class LoginEstandar_SAT {
    //variables
    public WebDriver driver;
    public Map<String, Object> vars;
    JavascriptExecutor js;
    CapturaPantalla print= new CapturaPantalla();
    String[] datos = null;

    //metodos


    public void login(WebDriver driver,String url,String usuario,String password ) throws IOException {

        driver.manage().window().maximize();
        js = (JavascriptExecutor) driver;
        vars = new HashMap<String, Object>();
        JavascriptExecutor js = (JavascriptExecutor) driver;

        //* MUESTRA LA URL A LA QUE SE ACCEDIO
        System.out.println("URL de acceso: "+driver.getCurrentUrl());
        print.print_Pantalla("inicio");
        //Asignación de tiemout en la carga de la página
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


        //// llamado a la lista de exxx

        //driver.manage().window().setSize(new Dimension(1382, 744));
        driver.get(url);
        driver.findElement(By.name("txt_usuario")).click();
        driver.findElement(By.name("txt_usuario")).sendKeys(usuario);
        print.print_Pantalla("ingresoUsuario");
        driver.findElement(By.name("txt_contrasenia")).click();
        driver.findElement(By.name("txt_contrasenia")).sendKeys(password);


        print.print_Pantalla("ingresocontraseña");
        driver.findElement(By.id("formulario_login_contenedor")).click();
        driver.findElement(By.id("btnIngresar")).click();
        driver.switchTo().frame(0);
        //VALIDAR SI UN OBJETO ES VISIBLE, si lo encuentra muestra TRUE
        Boolean Display= driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[1]/td[3]")).isDisplayed();
        System.out.println("Login exitoso,posición consolidada visibles:"+Display);
    }

}
