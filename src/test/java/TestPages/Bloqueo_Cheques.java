package TestPages;

import Globales.Reporte;
import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Bloqueo_Cheques {
    public void Bloqueo_Cheques()
    {
        PageFactory.initElements(Util.driver, this);
    }
    @FindBy(className = "tituloContenedor")
    WebElement titulo_pantalla=null;

    @FindBy(id="id_cuentas")
    WebElement select_cuenta = null;

    @FindBy(name="cbo_tipo_anula")
    WebElement select_tipo_anula=null;

    @FindBy(name = "txt_num_cheque")
    WebElement txt_num_cheq= null;

    @FindBy(name="txt_val_cheque")
    WebElement txt_val_cheque=null;

    @FindBy(name="cbo_causa_anula")
    WebElement select_causa_anula=null;

    @FindBy(name="cbo_lugar_giro")
    WebElement select_lugar_giro=null;

    @FindBy(name = "txt_nombre_benef")
    WebElement txt_nom_bene=null;

    @FindBy(name = "txt_comentario")
    WebElement txt_comentario=null;

    @FindBy(name="btn_boton")
    WebElement btn_continur=null;

    @FindBy(name="chk_acepto")
    WebElement chk_acepto=null;

    @FindBy(xpath="//*[@id=\"ejc_transaccion\"]/div/div[2]/form/table/tbody/tr[2]/td/div[2]/nobr/input[2]")
    WebElement btn_aceptar=null;

    @FindBy(name = "txttoken")
    WebElement txt_token=null;

    @FindBy(xpath="//*[@id=\\\"ejc_transaccion\\\"]/div/div[2]/form/table/tbody/tr[2]/td/div[2]/nobr/input[2]")
    WebElement btn_confirmar=null;

    String[] datos = null;

    public void CargaDatosBloqueoCheque(String sectrx) {
        datos = Util.getCamposDataPool(System.getProperty("user.dir")+"/Archivo/dp_bloqueo_cheques.txt",sectrx );
        if (datos == null)
            System.out.println("No hay datos en datapool");
        else
        {
            Util.setDataCliente(datos);
            System.out.println("setUp==>Datos Leidos de datapool");
        }
    }

    public void vp_opcion_cheque() {
        String actual = titulo_pantalla.getText();
        Util.assert_contiene("24online - Funcionalidad bloqueo de cheque", "Verificación que se encuentra en la pantalla de bloqueo de cheque", actual,"Bloqueo de Cheques", true, "N");
    }

    public  void navega() throws InterruptedException {
        Thread.sleep(4000);
       Util.driver.findElement(By.linkText("24online")).click();
       Util.driver.switchTo().defaultContent();
       Util.driver.switchTo().frame(1);
      //  Thread.sleep(4000);
        Util.driver.findElement(By.id("1001")).click();
        Util.driver.switchTo().defaultContent();
        Util.driver.switchTo().frame(2);
        Util.driver.findElement(By.linkText("Bloqueo de Cheques")).click();
    }
    public void ingreso_cuenta()
    {
        select_cuenta.sendKeys(Util.getDataCliente()[1]);
        Reporte.agregarPaso("Bloqueo de cheque", "Ingreso de cuenta", Util.getDataCliente()[1], "", true, "N");

    }




}
