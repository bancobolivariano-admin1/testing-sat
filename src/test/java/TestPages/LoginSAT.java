package TestPages;

import Globales.Reporte;
import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class LoginSAT {

    String[] datos = null;

    void CargaDatosLogin(String seclogin) {
        if (seclogin.equals("0"))
            seclogin = Util.prop.getProperty("sec_login");
        datos = Util.getCamposDataPool(System.getProperty("user.dir")+"/Archivo/dp_login.txt", seclogin);

        if (datos == null)
            System.out.println("No hay datos en datapool");
        else
        {
            Util.setDataCliente(datos);
            System.out.println("setUp==>Datos Leidos de datapool");
        }
    }

    public void vp_etiqueta_usuario() {
        WebElement etiq_usuario = Util.driver.findElement(By.xpath("//div[@class='etiqueta etiqueta_1']"));
        String actual = etiq_usuario.getText();
        Util.assert_contiene("LOGIN", "Verificación de etiqueta Usuario", actual,"Usuario", true, "N");
    }

    void ingreso_usuario()
    {
        WebElement txt_usario = Util.driver.findElement(By.name("txt_usuario"));
        txt_usario.click();
        txt_usario.sendKeys(Util.getDataCliente()[1]);
        Reporte.agregarPaso("LOGIN", "Ingreso de usuario", Util.getDataCliente()[1], "", true, "N");
    }

    void ingreso_password()
    {
        WebElement txt_contrasenia = Util.driver.findElement(By.name("txt_contrasenia"));
        txt_contrasenia.click();
        txt_contrasenia.sendKeys(Util.getDataCliente()[2]);
        Reporte.agregarPaso("LOGIN", "Ingreso de contraseña", Util.getDataCliente()[2], "", true, "N");
    }

    void click_login()
    {
        Util.driver.findElement(By.id("formulario_login_contenedor")).click();
        WebElement btnIngresar = Util.driver.findElement(By.id("btnIngresar"));
        String actual = btnIngresar.getAttribute("value");
        Util.assert_igual("LOGIN", "Click en botón Ingresar", actual, "Ingresar", false, "N");
        btnIngresar.click();
        Util.driver.switchTo().frame(0);
    }
    void vp_posicion_consolidada()
    {
        boolean mensaje= Util.driver.getPageSource().contains("Ultimo ingreso");
        String actual = "";
        if (mensaje=true) actual="Ok posición consolidada";
        else
            actual="Fallido";
        Util.assert_contiene("LOGIN", "Verificación de posición consolidada", actual,"Ok posición consolidada", true,"N");

    }
    void vp_fallido()
    {

        Util.driver.findElement(By.id("formulario_login_contenedor")).click();

        WebElement btnIngresar = Util.driver.findElement(By.id("btnIngresar"));
        String actual = btnIngresar.getAttribute("value");
        Util.assert_igual("LOGIN", "Click en botón Ingresar", actual, "Ingresar", false, "N");
        btnIngresar.click();

        boolean mensaje= Util.driver.getPageSource().contains("Usuario/Contraseña incorrecta");

        if (mensaje=true) actual="Login fallido";
        else
            actual="Exitoso";
        Util.assert_contiene("LOGIN", "Login fallido", actual,"Login fallido", true,"N");


    }
    void vp_posicionmultiempresa()
    {
        Util.driver.findElement(By.id("formulario_login_contenedor")).click();
        WebElement btnIngresar = Util.driver.findElement(By.id("btnIngresar"));
        String actual = btnIngresar.getAttribute("value");
        Util.assert_igual("LOGIN", "Click en botón Ingresar", actual, "Ingresar", false, "N");
        btnIngresar.click();

        Util.driver.findElement(By.id("formulario_login_contenedor")).click();

        //boolean mensaje= Util.driver.getPageSource().contains("EMPRESAS ASOCIADAS");

       // if (mensaje=true)

        Reporte.agregarPaso("LOGIN","Posicion consolidada de seleccion empresas","Selecciona empresa","",true,"N");
        WebElement btnIng = Util.driver.findElement(By.id("btn_boton"));
        btnIng.click();

        boolean mensaje= Util.driver.getPageSource().contains("Ultimo ingreso");
        String a = "";
        if (mensaje=true) a="Ok posición consolidada";
        else
            a="Fallido";
        Util.assert_contiene("LOGIN", "Verificación de posición consolidada", a,"Ok posición consolidada", true,"N");


    }
    public void Ingresar(String seclogin)
    {
        this.CargaDatosLogin(seclogin);
        this.vp_etiqueta_usuario();
        this.ingreso_usuario();
        this.ingreso_password();
        this.click_login();
        this.vp_posicion_consolidada();
    }

    public void Fallido(String seclogin)
    {
        this.CargaDatosLogin(seclogin);
        this.vp_etiqueta_usuario();
        this.ingreso_usuario();
        this.ingreso_password();
        this.vp_fallido();
        //this.vp_posicion_consolidada();
    }
    public void Ingresar_multiempresa(String seclogin)
    {
        this.CargaDatosLogin(seclogin);
        this.vp_etiqueta_usuario();
        this.ingreso_usuario();
        this.ingreso_password();
        this.vp_posicionmultiempresa();
    }

}
