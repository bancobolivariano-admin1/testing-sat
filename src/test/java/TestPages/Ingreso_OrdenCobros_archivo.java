package TestPages;

import Globales.CapturaPantalla;
import Globales.Reporte;
import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.Map;

public class Ingreso_OrdenCobros_archivo {
    private Map<String, Object> vars;
    JavascriptExecutor js;
    CapturaPantalla print = new CapturaPantalla();

public void clic_link_cobros()
{
    Util.driver.findElement(By.linkText("Cobros")).click();
    Util.driver.switchTo().defaultContent();
    Util.driver.switchTo().frame(1);
}

public void clic_link_carga()
{/*
    Util.driver.findElement(By.id("406")).click();
    Util.driver.switchTo().defaultContent();
    Util.driver.switchTo().frame(2);
*/
    Util.driver.switchTo().defaultContent();
    Util.driver.switchTo().frame(1);
    Util.waitForElementToBeClickable(By.id("406"));
    WebElement carga = Util.driver.findElement(By.id("406"));
    String actual = carga.getText();
    Util.assert_igual("SUBMENU", "Verificacion de etiqueta", actual,actual, true, "N");
    carga.click();
}
public void clic_link_ingresar()
{
    Util.driver.switchTo().defaultContent();
    Util.driver.switchTo().frame(2);
    Util.waitForElementToBeClickable(By.linkText("Ingresar"));
    WebElement ingresar = Util.driver.findElement(By.linkText("Ingresar"));
    String actual = ingresar.getText();
    Util.assert_contiene("SUBMENU","Verificacion de etiqueta",actual,"Ingresar",true,"N");
    ingresar.click();
    Util.AvanzarPagina();
}

public void selec_empresa(String cod_empresa)
{
    try {
        Thread.sleep(5000);
    } catch (InterruptedException exception) {
        exception.printStackTrace();
    }
    Util.waitForElementToBeClickable(By.name("cbo_empresa"));
    WebElement cbo_empresa = Util.driver.findElement(By.name("cbo_empresa"));
    cbo_empresa.click();
    cbo_empresa.sendKeys(cod_empresa + "\t");
    Util.assert_contiene("ORDEN DE COBRO", "Ingresa codigo empresa", cod_empresa, "", true, "N");

}

    public void selec_servicio(String nom_servicio)
    {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        Util.waitForElementToBeClickable(By.name("cbo_servicio"));
        WebElement cbo_servicio1 = Util.driver.findElement(By.name("cbo_servicio"));
        Select servicio = new Select(cbo_servicio1) ;
        servicio.selectByVisibleText(nom_servicio);
        Util.assert_contiene("ORDEN DE COBRO", "Selecciona servicio", nom_servicio, "", true, "N");

    }
    public void selec_cuenta(String num_cuenta)
    {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        Util.waitForElementToBeClickable(By.name("cbo_ctaautpg"));
        WebElement cbo_cuenta = Util.driver.findElement(By.name("cbo_ctaautpg"));
        Select cuenta = new Select(cbo_cuenta) ;
        cuenta.selectByVisibleText(num_cuenta);
        Util.assert_contiene("ORDEN DE COBRO", "Selecciona cuenta", num_cuenta, "", true, "N");
    }
    public void ingresa_referencia(String ref)
    {
        // Util.waitForElementToBeClickable(By.name("txt_referencia"));
        WebElement txt_referencia = Util.driver.findElement(By.name("txt_referencia"));
        txt_referencia.click();
        txt_referencia.sendKeys(ref);
        Reporte.agregarPaso("ORDEN DE COBRO", "Ingreso referencia", ref, "", true, "N");
        Util.AvanzarPagina();
    }
    public void carga_archivo(String carpeta,String nom_archivo) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }

        String sat= Util.driver.getWindowHandle();
        System.out.println("pantalla principal sat " + sat);
        //Almacena el ID de la ventana original
        String originalWindow =Util.driver.getWindowHandle();
        //Comprueba que no existen otras ventanas abiertas previamente
        assert Util.driver.getWindowHandles().size() == 1;
        //Haz clic en el enlace el cual abre una nueva ventana
        Util.driver.findElement(By.name("btnBoton2")).click();
        //Espera a la nueva ventana o pestaña
        //Recorrelas hasta encontrar el controlador de la nueva ventana
        for (String windowHandle : Util.driver.getWindowHandles()) {
            if(!originalWindow.contentEquals(windowHandle)) {
                Util.driver.switchTo().window(windowHandle);
                break;
            }
        }
        //Espera a que la nueva ventana cargue su contenido

        WebElement uploadelement = Util.driver.findElement(By.name("txt_file"));
        uploadelement.sendKeys("C:\\Users\\kmerinob\\Documents\\Lecho de Testeo\\ARCHIVOS\\"+carpeta+"\\"+nom_archivo);

        Util.driver.findElement(By.id("btn_cargar")).click();
        Util.driver.findElement(By.id("button1")).click();
        //*** volver a la pagina original
        //Cambia el controlador a la ventana o pestaña original
        Util.driver.switchTo().window(originalWindow);
        //Almacena el elemento web
        WebElement frame= Util.driver.findElement(By.id("oFrame"));

        //Cambia el foco al iframe
        Util.driver.switchTo().frame(frame);
        Reporte.agregarPaso("ORDEN DE COBRO", "Carga de archivo", nom_archivo, "", true, "N");
    }
    public void clic_generaorden()
    {
        WebElement btnGenerar = Util.driver.findElement(By.name("btnBoton3"));
        String actual = btnGenerar.getAttribute("value");
        Util.assert_igual("ORDEN DE COBRO", "Click en boton Generar Orden", actual, "Procesar", true, "N");
        btnGenerar.click();
        Util.AvanzarPagina();
    }
    public void vp_mensaje()
    {
        Util.driver.findElement(By.id("cuerpo")).click();
        Util.AvanzarPagina();

        boolean mensaje= Util.driver.getPageSource().contains("Verificado O.K.");
        if (mensaje)
            Reporte.agregarPaso("ORDEN DE COBRO", "Verificacion de mensaje de confirmacion", "Orden ingresada exitosamente", "", true, "N");
        else
            Reporte.agregarPaso("ORDEN DE COBRO", "Verificacion de mensaje de confirmacion", "ERROR", "", true, 1, "N");
        //como validar que la acción en la table ID=TABLE_CONSULTA_CARGA el resultado es Verificado O.K.
    }
    public void clic_transmitir(){
        try {
            Thread.sleep(6000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        Util.waitForElementToBeClickable(By.name("btnAceptar"));
        WebElement btn_transmitir = Util.driver.findElement(By.name("btnAceptar"));
        String actual = btn_transmitir.getAttribute("value");
        btn_transmitir.click();
        Reporte.agregarPaso("ORDEN DE COBRO","Transmitir orden de cobro","Transmitido","",true,"N");


    }


}
