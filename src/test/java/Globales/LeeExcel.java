package Globales;


import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LeeExcel {


    public static List  readExcel(String filepath,String sheetname) throws IOException
    {
        //String filepath= "E:\\SAT_maven\\DataPool\\001login_SAT.xlsx";

        String usuario= null;
        String clave= null;
        List list = new ArrayList();
        /* configuración para Apertura y lectura de archivo*/
        File file = new File(filepath); //AQUI SE LE ASIGNA A FILE LA RUTA DEL ARCHIVO
        FileInputStream inputStream =new FileInputStream(file);
        XSSFWorkbook newWorkbook = new XSSFWorkbook(inputStream);
        XSSFSheet newSheet =newWorkbook.getSheet(sheetname);
        //OBTIENE LA CANTIDAD DE FILAS QUE TIENE EL DATA POOL
        int rowCount =newSheet.getLastRowNum() - newSheet.getFirstRowNum()+1;
        System.out.println("cantidad de filas leidas: "+rowCount);
        for (int i=0;i < rowCount;i++ ) // recorre el archivo
        {
            XSSFRow row = newSheet.getRow(i);


                String Us = row.getCell(0).getRichStringCellValue().toString();
                String pass = row.getCell(1).getRichStringCellValue().toString();
//                String cuentas = row.getCell(2).getRichStringCellValue().toString();


            list.add(Us+"/"+pass+"/");
              //  usuario= row.getCell(j).getRichStringCellValue();
        }
        return list;

    }

    public static void main(String[] args) throws IOException {
        List lista = new ArrayList();
        String filepath= "E:\\AUT_SAT\\DataPool\\001login_SAT.xlsx";

        lista=readExcel(filepath,"Hoja1");

        System.out.println(lista.size());

        String[] parts = lista.get(2).toString().split("/");
        String usuario = parts[0]; // 123
        String password = parts[1]; // 654321


        System.out.println(usuario);
        System.out.println(password);

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        String usuario_1 = excel.getCellvalue(filepath, "Hoja1", 0, 0);
        String clave_2 = excel.getCellvalue(filepath, "Hoja1", 0, 1);

        System.out.println(usuario_1);
        System.out.println(clave_2);



    }
    public String getCellvalue(String filepath,String sheetname,int rownumber,int celnumber) throws IOException {
        /* configuración para Apertura y lectura de archivo*/
        File file = new File(filepath); //AQUI SE LE ASIGNA A FILE LA RUTA DEL ARCHIVO
        FileInputStream inputStream =new FileInputStream(file);
        XSSFWorkbook newWorkbook = new XSSFWorkbook(inputStream);
        XSSFSheet newSheet =newWorkbook.getSheet(sheetname);

        XSSFRow row= newSheet.getRow(rownumber); //objeto
        XSSFCell cell= row.getCell(celnumber);
        return cell.getStringCellValue();
    }

}
