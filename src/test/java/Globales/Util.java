package Globales;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;


public class Util {

    public static WebDriver driver;
    public static WebDriverWait wait;
    static JavascriptExecutor js;
    public static Properties prop = null;
    private static String[] dataCliente;

    public static String[] getDataCliente() {
        return dataCliente;
    }
    public static void setDataCliente(String[] datos) {
        Util.dataCliente = datos;
    }

    public static String ArchivoCaptura = "";

    static void InicioConfig() {
        prop = new Properties();
        try {
            prop.load(new FileInputStream(System.getProperty("user.dir")+"/config.properties"));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void Inicio(String nom_archivo, String ambiente) {
        InicioConfig();
        System.setProperty("webdriver.chrome.driver", prop.getProperty("ruta_driverchrome"));
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        js = (JavascriptExecutor) driver;
        //String ambiente_url =prop.getProperty("url_sitio");
        String url=null;
        switch (ambiente)
        {
            case "1": url = prop.getProperty("url_sitio1");
                break;
            case "2": url = prop.getProperty("url_sitio2");
                break;
            case "3": url = prop.getProperty("url_sitio3");
                break;
            case "4": url = prop.getProperty("url_sitio4");
                break;
            default: url =prop.getProperty("url_sitio1");
                break;
        }
        driver.get(url);
        driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
        Reporte.setNombreArchivo(nom_archivo);
        Reporte.setNombreReporte("");
        Reporte.setEntorno("");
    }

    public static List<String> getCamposDataPool(String fileName)
    {
        List<String> lines = Collections.emptyList();
        try
        {
            lines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return lines;
    }

    public static String[] getCamposDataPool(String fileName, String numregistro )
    {
        List<String> lines = Collections.emptyList();
        String[] campos = null;
        try
        {
            lines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
            Iterator<String> itr = lines.iterator();

            while (itr.hasNext())
            {
                campos = itr.next().split("\t");
                if (campos[0].equals(numregistro))
                {
                    break;
                }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return campos;
    }

    public static void CapturarImagen()
    {
        TimeZone tz = TimeZone.getTimeZone("EST"); // or PST, MID, etc ...
        Date now = new Date();
        DateFormat df = new SimpleDateFormat("yyyyMMddhhmmss");
        df.setTimeZone(tz);
        String currentTime = df.format(now);
        File file  = ((TakesScreenshot)Util.driver).getScreenshotAs(OutputType.FILE);
        //String Archivo = System.getProperty("user.dir") + "/capturas/" + "Img_" + currentTime + ".jpg";
        Util.ArchivoCaptura = System.getProperty("user.dir") + "/capturas/" + "Img_" + currentTime + ".jpg";;
        try {
            FileUtils.copyFile(file, new File(Util.ArchivoCaptura));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void AvanzarPagina()
    {
        JavascriptExecutor js  = (JavascriptExecutor) Util.driver;
        js.executeScript("window.scrollBy(0,1000)");
    }

    public static void RetrocederPagina()
    {
        JavascriptExecutor js  = (JavascriptExecutor) Util.driver;
        js.executeScript("window.scrollBy(0,0)");
    }

    public static void assert_igual(String caso, String detalle, String actual, String esperado, Boolean captura, String categoria)
    {
        try
        {
            assertThat(actual, is(esperado));
            Reporte.agregarPaso(caso, detalle, actual, esperado, captura, categoria);
        }
        catch (AssertionError e) {
            Reporte.agregarPaso(caso, detalle, e.getMessage(), esperado, captura, 1, categoria);
            System.out.println(e.getMessage());
            Assert.fail();
        }
    }

    public static void assert_contiene(String caso, String detalle, String actual, String esperado, Boolean captura, String categoria)
    {
        try
        {
            assertThat(actual, CoreMatchers.containsString(esperado));
            Reporte.agregarPaso(caso, detalle, actual, esperado, captura, categoria);
        }
        catch (AssertionError e) {
            Reporte.agregarPaso(caso, detalle, e.getMessage(), esperado, captura, 1, categoria);
            System.out.println(e.getMessage());
            Assert.fail();
        }
    }

    public static void assert_contiene(String caso, String detalle, String actual, String esperado1, String esperado2, Boolean captura, String categoria)
    {
        try
        {
            assertThat(actual, either(containsString(esperado1)).or(containsString(esperado2)));
            Reporte.agregarPaso(caso, detalle, actual, "", captura, categoria);
        }
        catch (AssertionError e) {
            Reporte.agregarPaso(caso, detalle, e.getMessage(), "", captura, 1, categoria);
            System.out.println(e.getMessage());
            Assert.fail();
        }
    }

    public static void assert_contiene(String caso, String detalle, String actual, String esperado1, String esperado2, String esperado3, Boolean captura, String categoria)
    {
        try
        {
            assertThat(actual, either(containsString(esperado1)).or(containsString(esperado2)).or(containsString(esperado3)));
            Reporte.agregarPaso(caso, detalle, actual, "", captura, categoria);
        }
        catch (AssertionError e) {
            Reporte.agregarPaso(caso, detalle, e.getMessage(), "", captura, 1, categoria);
            System.out.println(e.getMessage());
            Assert.fail();
        }
    }

    public static void waitForElementToBeClickable(By locator) {
        wait = new WebDriverWait(Util.driver,30,100);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }

}
