package Globales;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CapturaPantalla {

    //Metodo
    public void print_Pantalla(String nombre)
    {


        Rectangle rectangleTam = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
        try {
            Robot robot = new Robot();

            // tomamos una captura de pantalla( screenshot )
            BufferedImage bufferedImage = robot.createScreenCapture(rectangleTam);
            String nombreFichero=System.getProperty("user.home")+ File.separatorChar+"Documents\\Casos de Pruebas\\Captura_automatica\\"+nombre+ System.currentTimeMillis()+".jpg";
            System.out.println("Generando el fichero: "+nombreFichero );
            FileOutputStream out = new FileOutputStream(nombreFichero);
            // esbribe la imagen a fichero
            ImageIO.write(bufferedImage, "jpg", out);
        } catch (AWTException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
