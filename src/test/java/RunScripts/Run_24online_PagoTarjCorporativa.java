package RunScripts;

import Globales.LeeExcel;
import Globales.Reporte;
import Globales.Util;
import TestPages.LoginSAT;
import TestPages.PagoTarjetasCorporativas_24online;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class Run_24online_PagoTarjCorporativa {
    @Before
    public void iniciar_Chrome() {
        Util.Inicio("Tarjeta_Corporativa","2");
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Navegador: Chrome; " + " Version: 90.0.4430.24 (Official Build) (64-bit)"  );
    }
    @Test
    public void Test_Paga_Tarjeta_Corporativa() throws IOException {

        Reporte.setNombreReporte("Pago de tarjetas corporativas - 24online SAT");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("1");
        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\003pagotarjetacorporativa_SAT.xlsx";
        //String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 0);
        String muestra_tarj = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 0);
        String selec_tar = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 1);
        String muestra_cue = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 2);
        String selec_cue = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 3);
        String valor = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 4);
        String concepto = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 5);
        String token = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 6);

        PagoTarjetasCorporativas_24online corp = new PagoTarjetasCorporativas_24online();
        corp.click_link_24online();
        corp.click_link_tarjetacredito();
        corp.click_link_tarjetacorporativa();
        corp.vp_posicion_consolidada();
        corp.selecciona_tarjeta(muestra_tarj, selec_tar);
        corp.selecciona_pago();//submenu pago
        corp.selec_cuentadebito(muestra_cue,selec_cue);
        corp.ingresa_monto(valor);
        corp.ingresa_concepto(concepto);
        corp.click_continuar();
        corp.ingresa_token(token);
        corp.click_pagar();
        corp.valida_pagoOK();
    }
    @Test
    public void Test_Paga_Tarjeta_Corporativa_new() throws IOException {

        Reporte.setNombreReporte("Pago de tarjetas corporativas - 24online SAT");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("1");
        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\003pagotarjetacorporativa_SAT.xlsx";
        //String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 0);
        String muestra_tarj = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 0);
        String selec_tar = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 1);
        String muestra_cue = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 2);
        String selec_cue = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 3);
        String valor = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 4);
        String concepto = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 5);
        String token = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 6);

        PagoTarjetasCorporativas_24online corp = new PagoTarjetasCorporativas_24online();
        corp.click_link_24online();
        corp.click_link_tarjetacredito();
        corp.click_link_tarjetacorporativa();
        corp.vp_posicion_consolidada();
        corp.selecciona_tarjeta(muestra_tarj, selec_tar);
        corp.selecciona_pago();//submenu pago
        corp.selec_cuentadebito(muestra_cue,selec_cue);
        corp.ingresa_monto(valor);
        corp.ingresa_concepto(concepto);
        corp.click_continuar();
        corp.ingresa_token(token);
        corp.click_pagar();
        corp.valida_pagoOK();
    }
    @After
    public void finaliza()
    {
        Reporte.finReporte();
    }
}
