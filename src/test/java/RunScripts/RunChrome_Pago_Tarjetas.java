package RunScripts;

import Globales.Reporte;
import Globales.Util;
import TestPages.LoginEstandar_SAT;
import TestPages.LoginSAT;
import TestPages.PagoTarjetasCorporativas_24online;
import TestPages.PagodeTarjetasPropias;
import Globales.LeeExcel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import java.io.IOException;


public class RunChrome_Pago_Tarjetas {

    @Before
    public void iniciar_Chrome() {
        Util.Inicio("Tarjeta_Propias","2");
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Navegador: Chrome; " + " Version: 90.0.4430.24 (Official Build) (64-bit)"  );
    }

    @Test
    public void Test_Paga_Tarjeta_Propias_1() throws IOException {

        Reporte.setNombreReporte("Pago de Tarjetas Propias - 24online SAT");
        //EMPRESA LA LLAVE
        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("13");
        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\002pagotarjetaspropias_SAT.xlsx";
        String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 0);
        String selec_tar = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 1);
        String selec_cue = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 2);
        String valor = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 3);
        String concepto = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 4);
        String token = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 5);

        System.out.println(empresa);
        System.out.println(selec_tar);

        PagodeTarjetasPropias propias = new PagodeTarjetasPropias();
        propias.click_link_24online();
        propias.click_link_tarjetacredito();
        propias.selec_tarjeta(selec_tar);
        propias.selec_cuenta(selec_cue);
        propias.ingresa_monto(valor);
        propias.ingresa_concepto(concepto);
        propias.click_continuar();
        propias.ingresa_token(token);
        propias.click_pagar();
        propias.valida_pagoOK();

    }
    @Test
    public void Test_Paga_Tarjeta_Propias() throws IOException {

        Reporte.setNombreReporte("Pago de Tarjetas Propias - 24online SAT");
        //EMPRESA ARGOL
        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("18");
        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\002pagotarjetaspropias_SAT.xlsx";
        String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 0);
        String selec_tar = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 1);
        String selec_cue = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 2);
        String valor = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 3);
        String concepto = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 4);
        String token = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 5);

        System.out.println(empresa);
        System.out.println(selec_tar);

        PagodeTarjetasPropias propias = new PagodeTarjetasPropias();
        propias.click_link_24online();
        propias.click_link_tarjetacredito();
        propias.selec_tarjeta(selec_tar);
        propias.selec_cuenta(selec_cue);
        propias.ingresa_monto(valor);
        propias.ingresa_concepto(concepto);
        propias.click_continuar();
        propias.ingresa_token(token);
        propias.click_pagar();
        propias.valida_pagoOK();

    }
    @Test
    public void Test_Paga_Tarjeta_Propias_ADIC() throws IOException {

        Reporte.setNombreReporte("Pago de Tarjetas Propias Adicionales- 24online SAT");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("18");
        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\002pagotarjetaspropias_SAT.xlsx";
        String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 0);
        String selec_tar = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 1);
        String selec_cue = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 2);
        String valor = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 3);
        String concepto = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 4);
        String token = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 5);

        //System.out.println(empresa);
        System.out.println(selec_tar);

        PagodeTarjetasPropias propias = new PagodeTarjetasPropias();
        propias.click_link_24online();
        propias.click_link_tarjetacredito();
        propias.selec_tarjeta(selec_tar);
        propias.selec_cuenta(selec_cue);
        propias.ingresa_monto(valor);
        propias.ingresa_concepto(concepto);
        propias.click_continuar();
        propias.ingresa_token(token);
        propias.click_pagar();
        propias.valida_pagoOK();

    }
    @After
    public void finaliza()
    {
        Reporte.finReporte();
    }

}

