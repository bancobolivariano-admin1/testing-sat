package RunScripts;
import Globales.Reporte;
import Globales.Util;
import TestPages.*;
import Globales.LeeExcel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class Run_CargaAlumnos_Colegio {
    int fila=0;
    @Before
    public void iniciar_Chrome() {
        Util.Inicio("CargaAlumnos","1");
        Reporte.setEntorno("Ambiente: Desarrollo1" + "</b><br>" + "Navegador: Chrome; " + " Version: 88.0.4324.190 (Official Build) (64-bit)"  );
    }
    @Test
    public void Carga_AlumnosColeSinItem() throws IOException {

        //HABILITAR DRIVER DE NAVEGADOR CHROME
        Reporte.setNombreReporte("Carga de Alumnos de Colegios exitosa - Por carga de archivo sin items");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("20");
        // Pool de datos
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\012cargaAlumnosColegio.xlsx";
        //Indicar que fila del pool de datos leer
        fila=1;
       // String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 0);
       // System.out.println(empresa);
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", fila, 1);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", fila, 2);
        String carpeta=excel.getCellvalue(filepath_pgtarjeta,"Hoja1", fila,3);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", fila, 4);
        String archivo = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", fila, 5);

        Carga_Alumnos_Colegio carga= new Carga_Alumnos_Colegio();

        carga.Click_Menu_cobros();
        carga.Clic_link_Colegios();
        carga.Click_OpcionCargaalumnos();
        //carga.selec_empresa2(empresa);
        carga.selec_servicio(servicio);
        carga.selec_cuenta(cuenta);
        carga.carga_archivosinitem(carpeta,archivo);
        carga.ingresa_referencia(referencia);
        carga.clic_generaorden();
        carga.vp_mensaje();
        carga.clic_transmitir();

    }

    @Test
    public void Carga_AlumnosConItems() throws IOException {

        //HABILITAR DRIVER DE NAVEGADOR CHROME
        Reporte.setNombreReporte("Carga de Alumnos de Colegios exitosa - Por carga de archivo con Items");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("22");

        // Pool de datos
        //Indicar que fila del pool de datos leer
        fila=2;
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\012cargaAlumnosColegio.xlsx";
        String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", fila, 0);
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", fila, 1);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", fila, 2);
        String rutaArchivo=excel.getCellvalue(filepath_pgtarjeta,"Hoja1", fila,3);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", fila, 4);
        String archivo = excel.getCellvalue(filepath_pgtarjeta, "Hoja1",fila, 5);
        String archivoItems = excel.getCellvalue(filepath_pgtarjeta, "Hoja1",fila, 6);

        Carga_Alumnos_Colegio carga= new Carga_Alumnos_Colegio();

        carga.Click_Menu_cobros();
        carga.Clic_link_Colegios();
        carga.Click_OpcionCargaalumnos();
        carga.selec_servicio(servicio);
        carga.selec_cuenta(cuenta);
        carga.BuscaRuta_CargaArchivoConItems(rutaArchivo,archivo,archivoItems);
        carga.ingresa_referencia(referencia);
        carga.clic_generaorden();
        carga.vp_mensaje();
        carga.clic_transmitir();

    }


    @After
    public void finaliza()
    {
        Reporte.finReporte();
    }

}
