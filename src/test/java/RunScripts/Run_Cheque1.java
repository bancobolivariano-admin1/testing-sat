package RunScripts;

import Globales.LeeExcel;
import Globales.Reporte;
import Globales.Util;
import TestPages.Bloqueo_Cheques;
import TestPages.LoginEstandar_SAT;
import TestPages.LoginSAT;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;

public class Run_Cheque1 {
    private LeeExcel readFile; //objeto que lee
    WebDriver driver;
    @Before
    public void iniciar() {
        //Util.Inicio("Bloque_che");
      //  Util.Inicio("RunLogin");
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Navegador: Chrome; " + " Version: 88.0.4324.190 (Official Build) (64-bit)"  );
        System.setProperty("webdriver.chrome.driver", "E:\\Instaladores\\841\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void Bloqueo_Cheq_Individual() throws InterruptedException, IOException {
        Reporte.setNombreReporte("Bloqueo de Cheque individual");
        Bloqueo_Cheques cheque = new Bloqueo_Cheques();
        //LoginSAT login = new LoginSAT();
        //se envia como parametro la linea del registro a leer del data pool
       // login.Ingresar("3");
        //HABILITAR DRIVER DE NAVEGADOR CHROME
        String url = "http://sat2008desa2srv/banca_corporativa/pag_inicio.asp";
        //Pool de datos login
        String filepath = "E:\\AUT_SAT\\DataPool\\001login_SAT.xlsx";
        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        String usuario = excel.getCellvalue(filepath, "Hoja1", 4, 0);
        String clave = excel.getCellvalue(filepath, "Hoja1", 4, 1);

        LoginEstandar_SAT login = new LoginEstandar_SAT();
        login.login(driver, url, usuario, clave);
      /*  cheque.click_menu24online(driver);
        cheque.click_menucuentas(driver);
        cheque.click_menubloqueocheq(driver);
        cheque.vp_opción_cheque(driver);*/
    }
    @After
    public void salir()
    {
        Reporte.finReporte();
    }


}
