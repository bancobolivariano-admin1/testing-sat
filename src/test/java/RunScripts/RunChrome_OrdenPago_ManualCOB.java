package RunScripts;

import Globales.Reporte;
import Globales.Util;
import Globales.LeeExcel;
import TestPages.LoginSAT;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import TestPages.Ingreso_OrdenPago_manualCOB;
import java.io.IOException;

public class RunChrome_OrdenPago_ManualCOB {
    WebDriver driver;
    @Before
    public void iniciar_Chrome() {
        Util.Inicio("OrdenPagoCOB","1");
        Reporte.setEntorno("Ambiente: Desarrollo1" + "</b><br>" + "Navegador: Chrome; " + " Version: 88.0.4324.190 (Official Build) (64-bit)"  );
    }
    @Test
    public void Genera_ordenRolesExitosaCOB() throws IOException {
        //HABILITAR DRIVER DE NAVEGADOR CHROME
        Reporte.setNombreReporte("Generacion de Orden de pago Roles exitosa");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("5");

        // Pool de datos ingreso de orden forma de pago COB
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\004ordenpagoSAT_COB.xlsx";
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 0);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 1);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 2);
        String tipo_pago = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 3);
        Ingreso_OrdenPago_manualCOB orden_cob= new Ingreso_OrdenPago_manualCOB();
        orden_cob.click_link_pagos();
        orden_cob.click_link_carga();
        orden_cob.click_ingresamanual();
        orden_cob.selecciono_servicio(servicio);
        orden_cob.selecciono_cuenta(cuenta);
        orden_cob.ingreso_referencia(referencia);
        orden_cob.click_boton_ingresa();
        orden_cob.selecciono_tipopago(tipo_pago);

        // ingreso de detalle de ordenes-los lee de la hoja2 del datapool
        for (int i=1;i<=5 ;i++) {
            orden_cob.tipo_id_bene = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 0);
            orden_cob.cedula_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 1);
            orden_cob.nombre_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 2);
            orden_cob.concepto_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 3);
            orden_cob.tipo_cuenta_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 4);
            orden_cob.num_cuenta_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 5);
            orden_cob.valor = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 6);
            orden_cob.ingreso_datos_benef(i);
        }

        orden_cob.click_generar_orden();
        orden_cob.vp_mensaje();
    }
    @Test
    public void Genera_ordenProveedorExitosaCOB() throws  IOException {
        //HABILITAR DRIVER DE NAVEGADOR CHROME
        Reporte.setNombreReporte("Generacion de Orden de pago a Proveedores exitosa");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("5");

        // Pool de datos ingreso de orden forma de pago COB
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\004ordenpagoSAT_COB.xlsx";
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 0);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 1);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 2);
        String tipo_pago = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 3);

        Ingreso_OrdenPago_manualCOB orden_cob= new Ingreso_OrdenPago_manualCOB();
        orden_cob.click_link_pagos();
        orden_cob.click_link_carga();
        orden_cob.click_ingresamanual();
        System.out.println(servicio);
        orden_cob.selecciono_servicio(servicio);
        orden_cob.selecciono_cuenta(cuenta);
        orden_cob.ingreso_referencia(referencia);
        orden_cob.click_boton_ingresa();
        orden_cob.selecciono_tipopago(tipo_pago);
        // ingreso de detalle de ordenes-los lee de la hoja2 del datapool
        for (int i=1;i<=5 ;i++) {
        orden_cob.tipo_id_bene = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 0);
        orden_cob.cedula_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 1);
        orden_cob.nombre_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 2);
        orden_cob.concepto_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 3);
        orden_cob.tipo_cuenta_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 4);
        orden_cob.num_cuenta_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 5);
        orden_cob.valor = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 6);
        orden_cob.ingreso_datos_benef(i);
        }

        orden_cob.click_generar_orden();
        orden_cob.vp_mensaje();
    }
    @Test
    public void Genera_ordenTercerosExitosaCOB() throws IOException {
        //HABILITAR DRIVER DE NAVEGADOR CHROME
        Reporte.setNombreReporte("Generacion de Orden de pago a Terceros exitosa");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("4");

        // Pool de datos ingreso de orden forma de pago COB
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\004ordenpagoSAT_COB.xlsx";
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 0);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 1);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 2);
        String tipo_pago = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 3);

        Ingreso_OrdenPago_manualCOB orden_cob= new Ingreso_OrdenPago_manualCOB();
        orden_cob.click_link_pagos();
        orden_cob.click_link_carga();
        orden_cob.click_ingresamanual();
        System.out.println(servicio);
        orden_cob.selecciono_servicio(servicio);
        orden_cob.selecciono_cuenta(cuenta);
        orden_cob.ingreso_referencia(referencia);
        orden_cob.click_boton_ingresa();
        orden_cob.selecciono_tipopago(tipo_pago);

        // ingreso de detalle de ordenes-los lee de la hoja2 del datapool
        for (int i=1;i<=5 ;i++) {
            orden_cob.tipo_id_bene = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 0);
            orden_cob.cedula_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 1);
            orden_cob.nombre_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 2);
            orden_cob.concepto_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 3);
            orden_cob.tipo_cuenta_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 4);
            orden_cob.num_cuenta_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 5);
            orden_cob.valor = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 6);
            orden_cob.ingreso_datos_benef(i);
        }
        orden_cob.click_generar_orden();
        orden_cob.vp_mensaje();
    }

    @After
    public void finaliza()
    {
        Reporte.finReporte();
    }
}
