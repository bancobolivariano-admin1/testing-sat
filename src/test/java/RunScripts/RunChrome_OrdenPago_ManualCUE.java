package RunScripts;
import Globales.Reporte;
import Globales.Util;
import TestPages.Ingreso_OrdenPago_manualCUE;
import Globales.LeeExcel;
import TestPages.LoginSAT;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import java.io.IOException;

public class RunChrome_OrdenPago_ManualCUE {
    WebDriver driver;
    @Before
    public void iniciar_Chrome() {
        Util.Inicio("OrdenPagoCUE","1");
        Reporte.setEntorno("Ambiente: Desarrollo1" + "</b><br>" + "Navegador: Chrome; " + " Version: 90.0.4430.24 (Official Build) (64-bit)"  );
    }
    @Test
    public void Genera_ordenRolesExitosa() throws InterruptedException, IOException {

        //HABILITAR DRIVER DE NAVEGADOR CHROME
        Reporte.setNombreReporte("Generacion de Orden de pago Roles exitosa");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("5");
        // Pool de datos ingreso de orden forma de pago CUE
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\013ordenpagoSAT_CUE.xlsx";
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 0);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 1);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 2);
        String tipo_pago = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 3);
        Ingreso_OrdenPago_manualCUE orden_cue = new Ingreso_OrdenPago_manualCUE();
        orden_cue.click_link_pagos();
        orden_cue.click_link_carga();
        orden_cue.click_ingresamanual();
        orden_cue.selecciono_servicio(servicio);
        orden_cue.selecciono_cuenta(cuenta);
        orden_cue.ingreso_referencia(referencia);
        orden_cue.click_boton_ingresa();
        orden_cue.selecciono_tipopago(tipo_pago);

        // ingreso de detalle de ordenes-los lee de la hoja2 del datapool
        for (int i=1;i<=5 ;i++) {
            orden_cue.tipo_id_bene = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 0);
            orden_cue.cedula_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 1);
            orden_cue.nombre_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 2);
            orden_cue.concepto_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 3);
            orden_cue.tipo_cuenta_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 4);
            orden_cue.num_cuenta_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 5);
            orden_cue.valor = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 6);
            orden_cue.ingreso_datos_benef(i);

        }

        orden_cue.click_generar_orden();
        orden_cue.vp_mensaje();

    }
    @Test
    public void Genera_ordenProveedorExitosa() throws InterruptedException, IOException {

        //HABILITAR DRIVER DE NAVEGADOR CHROME
        Reporte.setNombreReporte("Generacion de Orden de pago Proveedor exitosa");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("5");
        // Pool de datos ingreso de orden forma de pago CUE
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\013ordenpagoSAT_CUE.xlsx";
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 0);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 1);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 2);
        String tipo_pago = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 3);
        Ingreso_OrdenPago_manualCUE orden_cue = new Ingreso_OrdenPago_manualCUE();
        orden_cue.click_link_pagos();
        orden_cue.click_link_carga();
        orden_cue.click_ingresamanual();
        orden_cue.selecciono_servicio(servicio);
        orden_cue.selecciono_cuenta(cuenta);
        orden_cue.ingreso_referencia(referencia);
        orden_cue.click_boton_ingresa();
        orden_cue.selecciono_tipopago(tipo_pago);

        // ingreso de detalle de ordenes-los lee de la hoja2 del datapool
        for (int i=1;i<=5 ;i++) {
            orden_cue.tipo_id_bene = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 0);
            orden_cue.cedula_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 1);
            orden_cue.nombre_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 2);
            orden_cue.concepto_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 3);
            orden_cue.tipo_cuenta_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 4);
            orden_cue.num_cuenta_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 5);
            orden_cue.valor = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 6);
            orden_cue.ingreso_datos_benef(i);

        }

        orden_cue.click_generar_orden();
        orden_cue.vp_mensaje();










    }
    @Test
    public void Genera_ordenTercerosExitosa() throws InterruptedException, IOException {

        //HABILITAR DRIVER DE NAVEGADOR CHROME
        Reporte.setNombreReporte("Generacion de Orden de pago Terceros exitosa");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("4");
        // Pool de datos ingreso de orden forma de pago CUE
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\013ordenpagoSAT_CUE.xlsx";
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 0);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 1);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 2);
        String tipo_pago = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 3);
        Ingreso_OrdenPago_manualCUE orden_cue = new Ingreso_OrdenPago_manualCUE();
        orden_cue.click_link_pagos();
        orden_cue.click_link_carga();
        orden_cue.click_ingresamanual();
        orden_cue.selecciono_servicio(servicio);
        orden_cue.selecciono_cuenta(cuenta);
        orden_cue.ingreso_referencia(referencia);
        orden_cue.click_boton_ingresa();
        orden_cue.selecciono_tipopago(tipo_pago);

        // ingreso de detalle de ordenes-los lee de la hoja2 del datapool
        for (int i=1;i<=5 ;i++) {
            orden_cue.tipo_id_bene = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 0);
            orden_cue.cedula_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 1);
            orden_cue.nombre_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 2);
            orden_cue.concepto_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 3);
            orden_cue.tipo_cuenta_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 4);
            orden_cue.num_cuenta_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 5);
            orden_cue.valor = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 6);
            orden_cue.ingreso_datos_benef(i);

        }

        orden_cue.click_generar_orden();
        orden_cue.vp_mensaje();










    }

    @After
    public void finaliza()
    {
        Reporte.finReporte();
    }

}
