package RunScripts;
import Globales.Util;
import Globales.LeeExcel;
import Globales.Reporte;
import TestPages.LoginSAT;
import org.junit.*;
import org.junit.Test;

public class Run_LoginSAT {
    private LeeExcel readFile; //objeto que lee

	//comentario Kevin
    @Before
    public void iniciar() {
        Util.Inicio("RunLogin", "3");
    }

   @Test
   public void LoginExitoso() {
       Reporte.setNombreReporte("LoginExitoso");
       LoginSAT login = new LoginSAT();
       //se envia como parametro la linea del registro a leer
       login.Ingresar("17");
   }
    @Test
    public void LoginExitosomultiempresa() {
        Reporte.setNombreReporte("Login Exitoso Usuario Multiempresa");
        LoginSAT login = new LoginSAT();
        //se envia como parametro la linea del registro a leer
        login.Ingresar_multiempresa("16");
    }
    @Test
    public void LoginFallido() {
        Reporte.setNombreReporte("LoginFallido");
        LoginSAT login = new LoginSAT();
        //se envia como parametro la linea del registro a leer
        login.Fallido("13");
    }

    @After
    public void salir()
    {
        Reporte.finReporte();
    }

    }


