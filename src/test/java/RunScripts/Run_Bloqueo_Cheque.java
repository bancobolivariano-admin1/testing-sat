package RunScripts;

import Globales.LeeExcel;
import Globales.Reporte;
import Globales.Util;
import TestPages.Bloqueo_Cheques;
import TestPages.Bloqueo_Cheques_24online;
import TestPages.LoginEstandar_SAT;
import TestPages.LoginSAT;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;

public class Run_Bloqueo_Cheque {
   WebDriver driver;

    @Before

    public void iniciar_Chrome() {
        System.setProperty("webdriver.chrome.driver", "E:\\Instaladores\\841\\chromedriver.exe");
        driver = new ChromeDriver();
        //Se envia el nombre del archivo del reporte
      //  Util.Inicio("RunLogin");
    }

     @Test
     public void Bloqueo_Cheque_Individual() throws InterruptedException, IOException
        {
           /*
            Reporte.setNombreReporte("Bloqueo de Cheque");
            LoginSAT login = new LoginSAT();
            //se envia como parametro la linea del registro a leer
            login.Ingresar("3");

            Bloqueo_Cheques bloqueo =new Bloqueo_Cheques();
            bloqueo.CargaDatosBloqueoCheque("1");
            bloqueo.navega();
            bloqueo.vp_opción_cheque();
            bloqueo.ingreso_cuenta();
*/
           //HABILITAR DRIVER DE NAVEGADOR CHROME
            String url = "http://sat2008desa2srv/banca_corporativa/pag_inicio.asp";
            //Pool de datos login
            String filepath = "E:\\AUT_SAT\\DataPool\\001login_SAT.xlsx";
            LeeExcel excel = new LeeExcel();//// instanciamos la clase
            String usuario = excel.getCellvalue(filepath, "Hoja1", 4, 0);
            String clave = excel.getCellvalue(filepath, "Hoja1", 4, 1);

            LoginEstandar_SAT login = new LoginEstandar_SAT();
            login.login(driver, url, usuario, clave);

            Bloqueo_Cheques_24online bloquear= new Bloqueo_Cheques_24online();
            bloquear.Bloqueo_Cheque(driver);
        }
    @After
    public void salir()
    {
        Reporte.finReporte();
    }


    }

