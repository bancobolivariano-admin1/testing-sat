package RunScripts;

import Globales.LeeExcel;
import Globales.Reporte;
import Globales.Util;
import TestPages.Ingreso_OrdenPago_ImpAduanero;
import TestPages.LoginSAT;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class Run_OrdenPagos_ImpAduanero {
    @Before
     public void iniciar_Chrome() {
        Util.Inicio("OrdenPago_ImpAduanero","2");
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Navegador: Chrome; " + " Version: 88.0.4324.190 (Official Build) (64-bit)"  );
    }
    @Test
    public void Genera_ordenPagosImpAduanero_FrmDebitoCtaCte() throws IOException {

        //Ingreso de orden con perfil empresa
        Reporte.setNombreReporte("Generacion de Orden de Pago IMPUESTOS ADUANEROS - Debito a cuenta corriente");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("8");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\009ordenpagoIMPADUANERO.xlsx";
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 0);
        String dia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 1);
        String mes = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 2);
        String anio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 3);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 4);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 5);
        String suministro = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 6);
        String ref2 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 7);
        String frm_pago = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 8);

        Ingreso_OrdenPago_ImpAduanero orden= new Ingreso_OrdenPago_ImpAduanero();
        orden.click_link_pagos();
        orden.click_link_carga();
        orden.clic_link_impaduaneros();
        System.out.println(cuenta);
        System.out.println(referencia);
        orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.clic_btningresar();
        orden.ingresa_suministro(suministro);
        orden.ingresa_nombene(ref2);
        orden.consulta_liquidacion();
        orden.genera_orden(frm_pago,cuenta);
        orden.vp_mensaje();

    }
    @Test
    public void Genera_ordenPagosImpAduanero_FrmDebitoCtaAho() throws IOException {

        //Ingreso de orden con perfil empresa
        Reporte.setNombreReporte("Generacion de Orden de Pago IMPUESTOS ADUANEROS - Debito a cuenta ahorro");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("19");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\009ordenpagoIMPADUANERO.xlsx";
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 0);
        String dia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 1);
        String mes = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 2);
        String anio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 3);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 4);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 5);
        String suministro = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 6);
        String ref2 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 7);
        String frm_pago = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 8);
        String ord_sap = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 9);

        Ingreso_OrdenPago_ImpAduanero orden= new Ingreso_OrdenPago_ImpAduanero();
        orden.click_link_pagos();
        orden.click_link_carga();
        orden.clic_link_impaduaneros();
        System.out.println(cuenta);
        System.out.println(referencia);
        orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.ingresa_ordensap(ord_sap);
        orden.clic_btningresar();
        orden.ingresa_suministro(suministro);
        orden.ingresa_nombene(ref2);
        orden.consulta_liquidacion();
        orden.genera_orden(frm_pago,cuenta);
        orden.vp_mensaje();

    }
    @Test
    public void Genera_ordenPagosImpAduanero_FrmEfectoCobro() throws IOException {

        //Ingreso de orden con perfil empresa
        Reporte.setNombreReporte("Generacion de Orden de Pago IMPUESTOS ADUANEROS - Efecto de Cobro");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("8");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\009ordenpagoIMPADUANERO.xlsx";
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 0);
        String dia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 1);
        String mes = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 2);
        String anio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 3);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 4);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 5);
        String suministro = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 6);
        String ref2 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 7);
        String frm_pago = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 8);

        Ingreso_OrdenPago_ImpAduanero orden= new Ingreso_OrdenPago_ImpAduanero();
        orden.click_link_pagos();
        orden.click_link_carga();
        orden.clic_link_impaduaneros();
        System.out.println(cuenta);
        System.out.println(referencia);
        orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.clic_btningresar();
        orden.ingresa_suministro(suministro);
        orden.ingresa_nombene(ref2);
        orden.consulta_liquidacion();
        orden.genera_orden(frm_pago,cuenta);
        orden.vp_mensaje();

    }
    @Test
    public void Genera_ordenPagosImpAduanero_FrmDebitoCtaCte_desa1() throws IOException {

        //Ingreso de orden con perfil empresa
        Reporte.setNombreReporte("Generacion de Orden de Pago IMPUESTOS ADUANEROS - Debito a cuenta corriente");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("10");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\009ordenpagoIMPADUANERO.xlsx";
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 0);
        String dia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 1);
        String mes = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 2);
        String anio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 3);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 4);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 5);
        String suministro = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 6);
        String ref2 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 7);
        String frm_pago = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 8);

        Ingreso_OrdenPago_ImpAduanero orden= new Ingreso_OrdenPago_ImpAduanero();
        orden.click_link_pagos();
        orden.click_link_carga();
        orden.clic_link_impaduaneros();
        System.out.println(cuenta);
        System.out.println(referencia);
        orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.clic_btningresar();
        orden.ingresa_suministro(suministro);
        orden.ingresa_nombene(ref2);
        orden.consulta_liquidacion();
        orden.genera_orden(frm_pago,cuenta);
        orden.vp_mensaje();

    }
    @After
    public void finaliza()
    {
        Reporte.finReporte();
    }
}
